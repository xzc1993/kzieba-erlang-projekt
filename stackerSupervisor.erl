-module(stackerSupervisor).
-bahaviour(supervisor).
-version('1.0').

-export([start/0, start_link/0, init/1]).

start()->
	{_, Pid} = start_link(),
	unlink(Pid).

start_link()->
	supervisor:start_link({local, stackerSupervisor}, stackerSupervisor, []).

init([])->
	process_flag(trap_exit, true),
	{ok,{
			{one_for_one, 20, 2000},
			[
				{stackerServer, {'Elixir.Stacker', start_link, []}, permanent, brutal_kill, worker, ['Elixir.Stacker']}
			]
		}
	}.