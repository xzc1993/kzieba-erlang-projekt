defmodule Elixirtest do

	def test(n) do
		:timer.tc( __MODULE__, :start_process, [0,n])
	end

	def start_process(m, n) do
		if m < n do
			spawn( __MODULE__, :init, [])
			start_process( m+1, n)
		else
			:ok
		end
	end

	def init() do
		:ok
	end


end