-module(hello).
-export([greet/0]).

greet() ->
	io:format("Erlang: Hello world!"),
	ok.