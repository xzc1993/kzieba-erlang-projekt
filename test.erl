-module(test).
-export([test/1, start_process/2, init/0]).

test(N)->
	timer:tc( ?MODULE, start_process, [0,N]).

start_process(M, N) when M < N ->
	spawn(?MODULE, init, []),
	start_process(M+1, N);

start_process(M, N) when M == N ->
	ok.

init()->
	ok.